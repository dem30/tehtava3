package Harkat;

import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Collections;
import java.util.Scanner;


class Laiva {
    protected String nimi;
    protected int pituus;
    protected int syvays;
    protected double nopeus;

    public Laiva(String nimi, int pituus, int syvays, double nopeus) {
        this.nimi = nimi;
        this.pituus = pituus;
        this.syvays = syvays;
        this.nopeus = nopeus;
    }

    public String getNimi() {
        return nimi;
    }

    public int getPituus() {
        return pituus;
    }

    public int getSyvays() {
        return syvays;
    }

    public double getNopeus() {
        return nopeus;
    }

    public void kesto() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Kuinka pitkä matka? (km)");
        double matka = Double.parseDouble(sc.nextLine());
        double nopeuskm = matka/1.852;
        double kesto = nopeuskm/this.nopeus;

        System.out.printf("Matkan kesto: %.2f tuntia",kesto);
        System.out.println();
        System.out.println("Laivan tiedot: ");
        System.out.println("Nimi: " + getNimi());
        System.out.println("Pituus: " + getPituus() + "m");
        System.out.println("Syväys: " + getSyvays() + "m");
        System.out.println("Nopeus: " + getNopeus() + " solmua");
        sc.close();
    }
}

class Rahtilaiva extends Laiva {
    protected double kapasiteetti;
    protected double rahti;

    public Rahtilaiva(String nimi, int pituus, int syvays, double nopeus, double kapasiteetti, double rahti){
        super(nimi, pituus, syvays, nopeus);
        this.kapasiteetti = kapasiteetti;
        this.rahti = rahti;
    }
    public double getKapasiteetti() {
        return kapasiteetti;
    }

    public double getNopeus(double nopeus, double rahti) {
        return (nopeus - (nopeus * (rahti/100)));
    }

    public double getRahti() {
        return rahti;
    }

    public void setKapasiteetti(double kapasiteetti) {
        this.kapasiteetti = kapasiteetti;
    }

    public void setRahti(double rahti) {
        if (rahti <= kapasiteetti) {
            this.rahti = rahti;
            this.nopeus = nopeus - (nopeus * (rahti/100));
        } else {
            System.out.println("Rahtia ei voi olla enemmän kuin kapasiteettia.");
        }
    }
}

class Autolautta extends Laiva {
    protected final int kapasiteetti;
    protected int matkustajamaara;
    protected int ajoneuvojenmaara;

    public Autolautta(String nimi, int pituus, int syvays, double nopeus, final int kapasiteetti, int matkustajamaara, int ajoneuvojenmaara) {
        super(nimi, pituus, syvays, nopeus);
        this.kapasiteetti = kapasiteetti;
        this.matkustajamaara = matkustajamaara;
        this.ajoneuvojenmaara = ajoneuvojenmaara;
    }

    public int getKapasiteetti() {
        return kapasiteetti;
    }

    public int getMatkustajamaara() {
        return matkustajamaara;
    }

    public void setMatkustajamaara(int matkustajamaara) {
        if (matkustajamaara <= kapasiteetti && matkustajamaara + (ajoneuvojenmaara * 10) <= kapasiteetti) {
            this.matkustajamaara = matkustajamaara;
        } else {
            System.out.println("Kapasiteetti ylittyi.");
        }
    }

    public int getAjoneuvojenmaara() {
        return ajoneuvojenmaara;
    }

    public void setAjoneuvojenmaara(int ajoneuvojenmaara) {
        if ((ajoneuvojenmaara * 10) <= kapasiteetti && matkustajamaara + (ajoneuvojenmaara * 10) <= kapasiteetti) {
            this.ajoneuvojenmaara = ajoneuvojenmaara;
        } else {
            System.out.println("Kapasiteetti ylittyi.");
        }
    }
}

